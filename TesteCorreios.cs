using System;
using System.IO;
using Xunit;
using Microsoft.Extensions.Configuration;
using AulaTesteAutomatizado.Config;
using System.Threading;

namespace AulaTesteAutomatizado
{
    public class TesteCorreios
    {

        private IConfiguration _configuration;
        public TesteCorreios()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"appsettings.json");
            _configuration = builder.Build();
        }

        // [Theory]
        // [InlineData(Browser.Chrome, "MT", "Varzea Grande", "Costa Verde")]
        // public void ConsultarPorBairro(Browser browser, string UF, string cidade, string bairro)
        // {
        //     TelaCorreios tela =
        //         new TelaCorreios(_configuration, browser);

        //     tela.CarregarPagina();
        //     Thread.Sleep(3000);
        //     tela.PreencherUF(UF);
        //     Thread.Sleep(3000);
        //     tela.PreencherLocalidade(cidade);
        //     Thread.Sleep(3000);
        //     tela.PreencherBairro(bairro);
        //     Thread.Sleep(3000);
        //     tela.ProcessarConsulta();
        //     Thread.Sleep(5000);

        //     tela.Fechar();

        //     //Assert.Equal(valorKm, resultado);
        // }

        [Theory]
        [InlineData(Browser.Chrome, "olhar digital")]
        public void ConsultarGoogle(Browser browser, string pesquisa)
        {
            TelaCorreios tela =
                new TelaCorreios(_configuration, browser);

            tela.CarregarPagina();

            tela.PreencherPesquisa(pesquisa);
            Thread.Sleep(3000);
            tela.ProcessarConsultaGoogle();
            Thread.Sleep(5000);

            tela.Fechar();

            //Assert.Equal(valorKm, resultado);
        }
    }
}
